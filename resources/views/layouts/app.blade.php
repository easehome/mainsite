@include('layouts.head')
<body id="top">
<div id="preloader"></div>
@include('layouts.menu')

@yield('content')

@if(!in_array( Request::path(), ['buy'] ) )
@include('layouts.footer')
@endif

@include('layouts.scripts')
</body>
</html>
