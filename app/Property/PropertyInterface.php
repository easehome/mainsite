<?php


namespace App\Property;


interface PropertyInterface
{
    public function yearBuilt(): ?int;

    public function areaSize(): ?int;

    public function yardSize(): ?int;

    public function estimatedPrice(): ?int;

    public function bathroomsCount(): ?int;

    public function bedroomsCount(): ?int;

    public function lastSoldPrice(): ?int;

    public function lastSoldDate(): ?string;

    public function taxAssessment(): ?float;

    public function taxAssessmentYear(): ?int;

    public function imageUrl(): ?string;

    public function images(): ?array;

    public function yearUpdated(): ?string;

    public function numFloors(): ?int;

    public function basement(): ?string;

    public function roof(): ?string;

    public function view(): ?string;

    public function parkingType(): ?string;

    public function heatingSources(): ?string;

    public function heatingSystem(): ?string;

    public function rooms(): ?string;
}
