@extends('layouts.cabinet')
@section('title', "Cabinet | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="settings">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title tal">
                        <h2 class="title">Your favorite houses</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @forelse($houses as $house)
                    <div class="col-12 col-lg-4">
                        <div class="single-funded">
                            <div class="img" style="background-image: url({{ $house['imgs'][0] ?? 'https://via.placeholder.com/400X300?text=iBuyershop' }})">
                                <span class="type">Single Family</span>
                            </div>
                            <div class="content">
                                <a href="{{ isset($house['alias']) ? route('cabinet.house_id',$house['alias']) : '#' }}" class="name">{{ $house['address'] }}</a>
                                <p class="adderss">Here is our estimate of your net proceeds.</p>
                                @if (isset($house['zestimate']))
                                    <h2 class="price_range">${{ $house['prices']['min'] }} - ${{ $house['prices']['max'] }}</h2>
                                @else
                                    <p class="status" style="margin-top: 20px;">Calculation pending...</p>
                                @endif
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-12">
                        <p class="text">You have not registered a house for sale yet</p>
                        <a href="{{ route('home') }}" class="home1-link link hvr-bs" style="color:#fff">Start search</a>
                    </div>
                @endforelse
            </div>
        </div>
    </section>
@endsection
