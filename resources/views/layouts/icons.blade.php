<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/icons/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/icons/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/icons/favicon-16x16.png')}}">
<link rel="manifest" href="{{ asset('/icons/site.webmanifest')}}">
<link rel="mask-icon" href="{{ asset('/icons/safari-pinned-tab.svg')}}" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="iBuyershop">
<meta name="application-name" content="iBuyershop">
<meta name="msapplication-TileColor" content="#00aba9">
<meta name="msapplication-TileImage" content="{{ asset('/icons/mstile-144x144.png')}}">
<meta name="theme-color" content="#ffffff">
