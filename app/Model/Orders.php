<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'first_name',
        'second_name',
        'phone',
        'email',
        'address',
        'type'
    ];
}
