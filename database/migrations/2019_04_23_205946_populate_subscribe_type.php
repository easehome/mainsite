<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class PopulateSubscribeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('subscribe_type')->insert([
            ['name' => 'All news'],
            ['name' => 'By house']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('subscribe_type')
            ->whereIn('name', ['All news', 'By house'])
            ->delete();
    }
}
