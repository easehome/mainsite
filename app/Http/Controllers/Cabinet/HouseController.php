<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Sells;
use App\Repository\RepositoryFactory;
use App\Http\Controllers\Controller;
use App\Traits\EstimateFormat;
use Illuminate\Support\Facades\Auth;

class HouseController extends Controller
{
    use EstimateFormat;

    public function index($house) {
        $house_in = Sells::where('user_id', Auth::user()->id)
                            ->where('alias','like', "%$house%")->first();

        //TODO page with 404
        if(!$house_in) return redirect(404);

        //TODO get info from Zillow
        $repository = RepositoryFactory::getRepository(RepositoryFactory::ZILLOW);
        $address = explode(',',$house_in->address)[0];
        $property = $repository->getProperty($address, null, null, $house_in->zip);

        $house = [
            'address' => $house_in->address,
            'lat' => (float) $property->latitude(),
            'lng' => (float) $property->longitude(),
            'bathrooms' => (float) $property->bathroomsCount(),
            'sqr' => (float) $property->areaSize(),
            'bedrooms' => (float) $property->bedroomsCount(),
            'imgs' => $property->images(),
            'alias' => $house_in->alias,
            'descr' => $property->descr(),
            'lastSoldPrice' => $property->lastSoldPrice(),
        ];

        if( $estimatedPrice = $property->estimatedPrice() ) $house['prices'] = $this->formatEstimate($estimatedPrice);

        $google_key = config('services.google.key');
        return view('cabinet.card', compact('house', 'property','google_key'));
    }
}
