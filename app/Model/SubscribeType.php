<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubscribeType extends Model
{
    protected $table = 'subscribe_type';
    protected $fillable = ['name'];
    public $timestamps = false;
}
