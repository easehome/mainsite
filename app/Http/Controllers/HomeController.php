<?php

namespace App\Http\Controllers;

use App\Model\Faq;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //faq format
        $faqs = [
            [
                'q' => 'What is iBuyershop?',
                'a' => 'ibuyershop search, easy sale of houses using our experience and technology',
            ],
        ];

        //houses format
        $houses = [
            [
                'address' => '9024 Villa Portofino Cir',
                'url' => '/9024-Villa-Portofino-Cir',
                'sub_address' => 'City, Zip, State',
                'img' => 'img/home-one/funded-2.jpg',
                'mark' => 'Single Family',
                'price' => number_format(140000),
            ],
            [
                'address' => '9024 Villa Portofino Cir',
                'url' => '/9024-Villa-Portofino-Cir',
                'sub_address' => 'City, Zip, State',
                'img' => 'img/home-one/funded-2.jpg',
                'mark' => 'Single Family',
                'price' => number_format(140000),
            ],
            [
                'address' => '9024 Villa Portofino Cir',
                'url' => '/9024-Villa-Portofino-Cir',
                'sub_address' => 'City, Zip, State',
                'img' => 'img/home-one/funded-2.jpg',
                'mark' => 'Single Family',
                'price' => number_format(140000),
            ],
        ];
        $houses = [];
        return view('home', compact('faqs','houses'));
    }
}
