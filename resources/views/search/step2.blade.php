@extends('layouts.app')
@section('title', "Home | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="step2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-xl-8 col-md-6">
                    <h1>We make offer for you</h1>
                    <p class="text">Thanks for your interest in selling with iBuyershop!</p>
                </div>
                <div class="col-12 col-xl-4 col-md-6">
                    <form action="{{ route('getoffer') }}" class="form-vertical" method="POST">
                        @csrf
                        <input type="hidden" name="address" value="{{ $address }}">
                        <div class="form-input-box">
                            <input name="first_name" type="text" placeholder="First name" class="" value="{{ old('first_name') }}">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="form-input-box">
                            <input name="second_name" type="text" placeholder="Last name" value="{{ old('second_name') }}">
                            @if ($errors->has('second_name'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('second_name') }}</span>
                            @endif
                        </div>
                        <div class="form-input-box">
                            <input name="email" type="email" placeholder="Email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-input-box">
                            <input name="phone" type="tel" placeholder="Phone" value="{{ old('phone') }}">
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="form-input-box btn-box">
                            <button type="submit" class="btn">Create offer for me</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
