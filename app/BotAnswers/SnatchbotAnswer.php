<?php


namespace App\BotAnswers;


class SnatchbotAnswer implements BotAnswer
{
    use BotAnswerFormater;

    public function __construct(array $properties)
    {
        foreach($properties as $k => $v) {
            $this->{$k} = $v;
        }
        $this->cards = [];
        return $this;
    }

    public function addMessageText(string $text, array $buttons = [] )
    {
        $this->cards[] = [
            'type' => 'text',
            'value' => $text,
            'buttons' => $buttons
        ];
        return $this;
    }

    public function addMessageGallery(array $images, array $buttons = [])
    {
        $elements = collect($images)->map(function($item) {
            return [
                'heading' => $item['title'],
                'image' => $item['url']
            ];
        });
        $this->cards[] = [
            'type'               => 'gallery',
            'value'              => 'gallery',
            'gallery'           => $elements
        ];
        return $this;
    }

    public function addMessageImage(string $img, array $buttons = [] )
    {
        $this->cards[] = [
            'type' => 'image',
            'value' => $img
        ];
        return $this;
    }

    public function addQuickReplies(string $caption, string $target)
    {
        $this->suggested_replies = [];
        $this->suggested_replies[] = $caption;
        return $this;
    }
}
