<section class="invest_metriks">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Key Metrics</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-sm-6 col-md-3">
                <div class="metrik">
                    <h4>200k</h4>
                    <p>Homes<br>in database</p>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-3">
                <div class="metrik">
                    <h4>149</h4>
                    <p>Towns<br>in database</p>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-3">
                <div class="metrik">
                    <h4>7</h4>
                    <p>Employees</p>
                </div>
            </div>
        </div>
    </div>
</section>
