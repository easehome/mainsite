<?php

namespace App\Http\Controllers;

use App\Http\Requests\VendorsRequest;
use App\Model\Vendors;

class VendorsController extends Controller
{
    public function index() {
        return view('pages.vendors');
    }

    public function getoffer(VendorsRequest $request) {

        Vendors::create($request->all());

        return response()->json([
            'status' => 200,
            'response' => __('vendors.success')
        ]);
    }
}
