<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    protected $table = 'vendors';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'about',
        'status'
    ];
}
