<?php

namespace App\Http\Controllers;

use App\Order\OrderHelper;
use App\Repository\RepositoryFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;

class SearchController extends Controller
{
    public function showSearch()
    {
        return view('search');
    }

    public function execSearch(Request $request)
    {
        /* ? TODO: replace factory with service container */
        $repository = RepositoryFactory::getRepository(RepositoryFactory::ZILLOW);
        $zillowProperty = $repository->getProperty($request->address, $request->city, $request->state);

        $repository = RepositoryFactory::getRepository(RepositoryFactory::MLS);
        $mlsProperty = $repository->getProperty($request->address, $request->city, $request->state);

        return view('search', compact('zillowProperty', 'mlsProperty'));
    }

    public function step1(Request $request) {

        $this->validate($request, [
            'postal_code' => 'required|string',
            'country' => 'required|string',
            'administrative_area_level_1' => 'required|string',
            'locality' => 'required|string',
            'route' => 'required|string',
            'street_number' => 'required|string',
            'address' => 'required|string',
        ]);

        $repository = RepositoryFactory::getRepository(RepositoryFactory::ZILLOW);
        $zillowProperty = $repository->getProperty("{$request->street_number} {$request->route}", null, null, $request->postal_code);

        if (! $zillowProperty) {
            Session::put('notfound',1);
            return view('cabinet.house.notfound', ['address' => $request->address]);
        }

        $address = [
            'street' => $request->address,
            'lat' => $zillowProperty->latitude(),
            'lng' => $zillowProperty->longitude(),
            'zip' => $request->postal_code,
            'state' => $request->administrative_area_level_1,
            'address' => "{$request->street_number} {$request->route}"
        ];

        Session::put('address', $address);

        return redirect()->route('correct_address_show');

    }

    public function step1show() {
        $address = Session::get('address');
        $google_key = config('services.google.key');
        return view('search.step1', compact('address','google_key'));
    }

    public function step2(Request $request) {
        return redirect()->route('offer_me_show');

    }

    public function step2show() {
        $address = Session::get('address');
        $address = $address['street'];

        if (Auth::check()) {

            $user = Auth::user();

            // create order
            OrderHelper::createOrder($user, $address);

            //create sells
            $url = OrderHelper::createSell($user, $address);

            //TODO redirect to card
            return redirect()->route('cabinet.house_id', $url);
        } else {
            return view('search.step2', compact('address'));
        }
    }
}
