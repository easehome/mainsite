@extends('layouts.app')

@section('title', "Vendors | ".env('APP_NAME'))
@section('description', "description")

@section('content')
    <section class="vendors">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-5 col-lg-6">
                    <div class="page-title title-left">
                        <h2 class="title">Increase your profit with us iBuyershop</h2>
                        <p class="text">We conduct a constant set of vendors and contractors in our team</p>
                        <button type="button" href="#contact_us" class="btn smoothscroll">Become a vendor</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="vendors_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Service areas</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-12 col-md-5">
                    <div class="vendors_area--text">
                        <p class="text">We are looking for vendors who will help to inspect, repair and repair the houses we have purchased for one family.</p>
                        <h4>We specialize in Florida and soon in other States</h4>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <img src="img/map.jpg" class="img-fluid vendors_area_img" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="vendors_ares">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">We need vendors in the following areas:</h2>
                    </div>
                </div>
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="vendors_ares_tags">
                        <ul class="tags">
                            <li>Electrical</li>
                            <li>Flooring</li>
                            <li>Foundation</li>
                            <li>General Contractors</li>
                            <li>Handyman</li>
                            <li>Home Cleaners</li>
                            <li>Landscaping</li>
                            <li>Photography</li>
                            <li>Pest & Termite</li>
                            <li>Plumbing</li>
                            <li>Roofing</li>
                            <li>Septic</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="vendors_contact" id="contact_us">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Contact with us</h2>
                        <p class="text">Send request with a description of your business and we will contact you.</p>
                    </div>
                </div>
                <div class="col-12 col-lg-8 offset-lg-2">
                    <form class="sign_form vendor_offer" method="POST" action="{{ route('vendors_order') }}">
                        @csrf
                        <div class="form_row_three">
                            <div class="form-group">
                                <input  type="text" class="form-control" name="name" value="{{ old('name') }}" required="" placeholder="Full name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required="" placeholder="Phone">
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="about" id="" cols="30" rows="10" placeholder="A little about you"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).on('submit','.vendor_offer', function(e) {
                e.preventDefault();
                var form = $(this);
                if( form.find('[type=email]').val() < 2 ) {
                    alert('no email');
                    return;
                }

                $.ajax({
                    url: $(this).prop('action'),
                    type:'post',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        form.find('[type=submit]').prop('disabled',true);
                    },
                    success: function(response) {
                        if(response.response) {
                            showModal(1,response.response);
                        }
                        if(response.error) {
                            showModal(0,response.error);
                        }
                    },
                    error: function() {
                        console.log('something went wrong');
                    },
                    complete: function() {
                        form.find('[type=submit]').prop('disabled',false);
                        form.trigger('reset');
                    }
                })
            });
        </script>
    @endpush

@endsection
