@extends('layouts.app')

@section('title', "Investor Relations | ".env('APP_NAME'))
@section('description', "description")

@section('content')
    <section class="investors_header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="investors_header_fl">
                        <h1>iBuyershop investor Relations</h1>
                        <a class="link btn-style-1 hvr-bs smoothscroll" href="#invest_request">Request investment materials</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.sections.about')

    @include('layouts.sections.metriks')

    <section class="invest_posts">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title tal">
                        <h2 class="title">Press Releases</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="#" class="">
                        <div class="single-feature" style="">
                            <div class="content">
                                <p class="info">Feb 24, 2019</p>
                                <h4 class="price">Do I need homeowners insurance?</h4>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="#" class="">
                        <div class="single-feature" style="">
                            <div class="content">
                                <p class="info">Feb 24, 2019</p>
                                <h4 class="price">Do I need homeowners insurance?</h4>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="#" class="">
                        <div class="single-feature" style="">
                            <div class="content">
                                <p class="info">Feb 24, 2019</p>
                                <h4 class="price">Do I need homeowners insurance?</h4>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="#" class="">
                        <div class="single-feature" style="">
                            <div class="content">
                                <p class="info">Feb 24, 2019</p>
                                <h4 class="price">Do I need homeowners insurance?</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="#" class="link btn-style-1 hvr-bs">View more</a>
                </div>
            </div>
        </div>
    </section>

    <section id="invest_request" class="invest_request">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Request investment materials</h2>
                    </div>
                </div>
                <div class="col-12 col-lg-8 offset-lg-2">
                    <form class="sign_form invest_offer" method="POST" action="{{ route('investors_post') }}">
                        @csrf
                        <div class="form_row_three">
                            <div class="form-group">
                                <input  type="text" class="form-control" name="name" value="{{ old('name') }}" required="" placeholder="Full name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required="" placeholder="Phone">
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).on('submit','.invest_offer', function(e) {
                e.preventDefault();
                var form = $(this);
                $.ajax({
                    url: $(this).prop('action'),
                    type:'post',
                    data: $(this).serialize(),
                    beforeSend: function() {
                        form.find('[type=submit]').prop('disabled',true);
                    },
                    success: function(response) {
                        if(response.response) showModal(1,response.response);
                        if(response.error) showModal(0,response.error);
                    },
                    error: function() {
                        console.log('something went wrong');
                    },
                    complete: function() {
                        form.find('[type=submit]').prop('disabled',false);
                        form.trigger('reset');
                    }
                })
            });
        </script>
    @endpush

@endsection
