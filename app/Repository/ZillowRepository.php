<?php


namespace App\Repository;


use App\Property\PropertyInterface;
use App\Property\ZillowMapper;
use Arr;
use Cache;
use Log;
use GuzzleHttp\Client;

class ZillowRepository implements PropertyRepositoryInterface
{
    const BASE_URI = 'http://www.zillow.com/webservice/';

    private $zwsId;

    public function __construct(string $zwsId)
    {
        $this->zwsId = $zwsId;
    }

    public function getProperty(string $address, $city, $state, $zip = null): ?PropertyInterface
    {
        $xml = $this->request('GetDeepSearchResults.htm', [
            'address' => $address,
            'citystatezip' => "$city $state $zip"
        ]);

        // No results
        if ((string) $xml->message->code !== '0') {
            return null;
        }

        $xml2 = $this->request('GetUpdatedPropertyDetails.htm', [
            'zpid' => (int) Arr::first($xml->xpath('//result'))->zpid,
        ]);

        return new ZillowMapper(
            Arr::first($xml->xpath('//result')),
            (string) $xml2->message->code === '0' ? Arr::first($xml2->xpath('//response')) : null
        );
    }

    protected function request($uri, $params): \SimpleXMLElement
    {
        $cacheKey = __METHOD__ . json_encode(func_get_args());

        if ($cachedXml = Cache::get($cacheKey)) {
            return new \SimpleXMLElement($cachedXml);
        }

        if (empty($this->zwsId)) {
            throw new Exception('ZWS id not set.');
        }

        $uri = self::BASE_URI . $uri;
        $params['zws-id'] = $this->zwsId;
        $params = array_map(function($item) {
            return urlencode($item);
        }, $params);

        $client = new Client;
        $res = $client->request('GET', $uri . '?' . http_build_query($params));
        $xml = (string) $res->getBody();

        Log::debug(__METHOD__, [$uri . '?' . http_build_query($params), $xml]);

        Cache::set($cacheKey, $xml, (60 * 24 * config('services.zillow.caching_days')));

        return new \SimpleXMLElement($xml);
    }
}
