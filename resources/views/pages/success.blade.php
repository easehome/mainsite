@extends('layouts.app')

@section('title', "vendors iBuyershop | ".env('APP_NAME'))
@section('description', "Описание")

@section('content')
    <section class="success">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">
                            {{ $title }}
                        </h2>
                        <p class="text">
                            {{ $text }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="{{ route('home') }}" style="" class="menu-item button  home1-link link hvr-bs">Back to home</a>
                </div>
            </div>
        </div>
    </section>
@endsection
