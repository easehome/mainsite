@extends('layouts.cabinet')
@section('title', "Settings | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="settings">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title tal">
                        <h2 class="title">Settings</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="settings_block">
                        <h3>Change password</h3>
                        <form action="{{ route('cabinet.change_password') }}" method="POST" class="settings_change">
                            @csrf
                            <div class="form-group">
                                <input type="password" name="password" placeholder="New password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" placeholder="Confirm New Password">
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Change password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(session('success'))
        @push('scripts')
        <script>
            showModal(1, '{{ session('success') }}');
        </script>
        @endpush
    @endif
@endsection
