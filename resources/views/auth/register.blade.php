@extends('layouts.app')
@section('title', "Sing up | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="register signs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Sign up</h2>
                        <p class="text">Be with us</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 offset-lg-4">
                    <form class="sign_form" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required placeholder="First name">

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('first_name') }}</span>
                                @endif
                        </div>
                        <div class="form-group">
                            <input id="second_name" type="text" class="form-control{{ $errors->has('second_name') ? ' is-invalid' : '' }}" name="second_name" value="{{ old('second_name') }}" required placeholder="Second name">

                            @if ($errors->has('second_name'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('second_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
