<?php

return [
    'phone' => [
        'full' => '+19739062203',
        'format' => '+1 (973) 906-2203'
    ],
    'mail' => [
        'info' => 'ask@ibuyershop.com'
    ],
    'address' => '18401 Collins Ave, Sunny Isles Beach, FL 33160',
    'social' => [
        'fb' => [
            'icon' => 'fa-facebook-f',
            'link' => 'https://fb.com'
        ],
        'in' => [
            'icon' => 'fa-linkedin-in',
            'link' => 'https://fb.com'
        ],
        'tg' => [
            'icon' => 'fa-telegram',
            'link' => 'https://fb.com'
        ],
        'tw' => [
            'icon' => 'fa-twitter',
            'link' => 'https://fb.com'
        ],
        'gp' => [
            'icon' => 'fa-google-plus-g',
            'link' => 'https://fb.com'
        ],
    ],
    'order' => 'grvoyt@ya.ru', //separate with coma
    'emails' => [
        'press'     => 'pr@ibuyershop.com',
        'vendors'   => 'vendor@ibuyershop.com',
        'career'    => 'career@ibuyershop.com',
        'investors' => 'invest@ibuyershop.com',
    ],
    'offices' => [
        'Florida' => [
            'address' => '18401 Collins Ave, Sunny Isles Beach, FL 33160',
            'email'   => 'florida@ibuyershop.com',
            'phone'   => '+1 (973) 906-2203'
        ],
        'Other states coming soon' => [
            'address' => 'Address will be added.',
            'email'   => 'ask@ibuyershop.com',
            'phone'   => '+1 (973) 906-2203'
        ]
    ]
];
