<?php


namespace App\BotAnswers;


class ManychatAnswer implements BotAnswer
{
    use BotAnswerFormater;
    public function __construct()
    {
        $this->response = [
            'version' => 'v2',
            'content' => []
        ];
        return $this;
    }

    /**
     * @param string $text
     * @param array $buttons
     * @return $this
     */
    public function addMessageText(string $text, array $buttons = [] ) {
        $this->response['content']['messages'][] = [
            'type'    => 'text',
            'text'    => $text,
            'buttons' => $buttons
        ];
        return $this;
    }

    /**
     * @param array $images
     * @param array $buttons
     * @return $this
     */
    public function addMessageGallery(array $images, array $buttons = []) {
        $elements = collect($images)->map(function($item) {
            return [
                'title' => $item['title'],
                'image_url' => $item['url']
            ];
        });
        $this->response['content']['messages'][] = [
            'type'               => 'cards',
            'elements'           => $elements,
            'image_aspect_ratio' => 'horizontal'
        ];
        return $this;
    }

    /**
     * @param string $img
     * @param array $buttons
     * @return $this
     */
    public function addMessageImage(string $img, array $buttons = [] ) {
        $this->response['content']['messages'][] = [
            'type' => 'image',
            'url'  => $img,
            'buttons' => $buttons
        ];
        return $this;
    }

    /**
     * @param string $caption
     * @param string $target
     * @return $this
     */
    public function addQuickReplies(string $caption, string $target) {
        $this->response['content']['quick_replies'][] = [
            'type'    => 'node',
            'caption' => $caption,
            'target'  => $target
        ];

        return $this;
    }
}
