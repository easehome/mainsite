<?php


namespace App\Property;

use Arr;

class MLSMapper implements PropertyInterface
{
    private $listing;

    public function __construct(array $listing)
    {
        $this->listing = $listing;
    }

    public function yearBuilt(): ?int
    {
        return $this->string('YearBuilt');
    }

    public function areaSize(): ?int
    {
        // Fields options: ApproxSqftTotalArea, BuildingSqFt, LotSqFootage, PropertySqFt, SqFtLivArea, SqFtTotal
        return $this->string('PropertySqFt');
    }

    public function yardSize(): ?int
    {
        return null;
    }

    public function estimatedPrice(): ?int
    {
        return null;
    }

    public function bathroomsCount(): ?int
    {
        return $this->string('BathsFull');
    }

    public function bedroomsCount(): ?int
    {
        return $this->string('BedsTotal'); // TODO: check if it is the same as bedrooms
    }

    public function lastSoldPrice(): ?int
    {
        return $this->string('LastListPrice');
    }

    public function lastSoldDate(): ?string
    {
        return null; // No data in MLS
    }

    public function taxAssessment(): ?float
    {
        // TODO: validate dependency on TaxInformation
        return $this->string('TaxAmount');
    }

    public function taxAssessmentYear(): ?int
    {
        return $this->string('TaxYear');
    }

    public function imageUrl(): ?string
    {
        // TODO: Implement imageUrl() method.
        return null;
    }

    public function images(): ?array
    {
        // TODO: Implement images() method.
        return null;
    }

    public function yearUpdated(): ?string
    {
        return null; // No data in MLS
    }

    public function numFloors(): ?int
    {
        return $this->string('NumFloors');
    }

    public function basement(): ?string
    {
        return null; // No data in MLS
    }

    public function roof(): ?string
    {
        return $this->string('Roof') ?? $this->string('RoofDescription');
    }

    public function view(): ?string
    {
        return $this->string('UnitView') ?? $this->string('View') ?? $this->string('WaterView');
    }

    public function parkingType(): ?string
    {
        return $this->string('ParkingDescription');
    }

    public function heatingSources(): ?string
    {
        return $this->string('HeatingDescription');
    }

    public function heatingSystem(): ?string
    {
        return null; // No data in MLS
    }

    public function rooms(): ?string
    {
        return $this->string('RoomCount');
    }

    public function jsonSerialize()
    {
        return [
            'yearBuilt' => $this->yearBuilt(),
            'areaSize' => $this->areaSize(),
            'yardSize' => $this->yardSize(),
            'bathroomsCount' => $this->bathroomsCount(),
            'bedroomsCount' => $this->bedroomsCount(),
            'lastSoldPrice' => $this->lastSoldPrice(),
            'lastSoldDate' => $this->lastSoldDate(),
            'taxAssessment' => $this->taxAssessment(),
            'taxAssessmentYear' => $this->taxAssessmentYear(),
            'numFloors' => $this->numFloors(),
            'basement' => $this->basement(),
            'roof' => $this->roof(),
            'view' => $this->view(),
            'parkingType' => $this->parkingType(),
            'heatingSources' => $this->heatingSources(),
            'heatingSystem' => $this->heatingSystem(),
            'rooms' => $this->rooms(),
        ];
    }

    protected function string($name)
    {
        $value = Arr::get($this->listing, $name);
        return !in_array(trim($value), ['', 'None']) ? $value : null;
    }
}
