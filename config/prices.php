<?php

return [
    'percent' => [
        'min' => env('PRICE_PERCENT_MIN',5),
        'max' => env('PRICE_PERCENT_MAX',15)
    ],
];
