@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Property search</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                     <form action="/search" method="POST">
                         <div class="form-group">
                            <input class="form-control" name="address" placeholder="Address" value="{{ request()->address }}">
                         </div>
                         <div class="form-group">
                            <input class="form-control" name="city" placeholder="City" value="{{ request()->city }}">
                         </div>
                         <div class="form-group">
                            <input class="form-control" name="state" placeholder="State" value="{{ request()->state }}" placeholder="FL">
                         </div>
                         {!! csrf_field() !!}
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if (Route::is('search-results'))

        <div class="row justify-content-center" style="margin-top: 30px;">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Zillow data</div>

                    <div class="card-body">

                        @if (! empty($zillowProperty))
                            Year Built: {{ $zillowProperty->yearBuilt() }}<br>
                            Area Size: {{ $zillowProperty->areaSize() }}<br>
                            Yard Size: {{ $zillowProperty->yardSize() }}<br>
                            Estimated Price: {{ $zillowProperty->estimatedPrice() }}<br>
                            Bedrooms Count: {{ $zillowProperty->bedroomsCount() }}<br>
                            Bathrooms Count: {{ $zillowProperty->bathroomsCount() }}<br>
                            Last Sold Date: {{ $zillowProperty->lastSoldDate() }}<br>
                            Last Sold Price: {{ $zillowProperty->lastSoldPrice() }}<br>
                            Tax Assessment: {{ $zillowProperty->taxAssessment() }}<br>
                            Tax Assessment Year: {{ $zillowProperty->taxAssessmentYear() }}<br>
                            Image: <img src="{{ $zillowProperty->imageUrl() }}"><br>
                        @else
                            Nothing found
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">MLS data</div>

                    <div class="card-body">

                        @if (! empty($mlsProperty))
                            Year Built: {{ $mlsProperty->yearBuilt() }}<br>
                            Area Size: {{ $mlsProperty->areaSize() }}<br>
                            Yard Size: {{ $mlsProperty->yardSize() }}<br>
                            Estimated Price: {{ $mlsProperty->estimatedPrice() }}<br>
                            Bedrooms Count: {{ $mlsProperty->bedroomsCount() }}<br>
                            Bathrooms Count: {{ $mlsProperty->bathroomsCount() }}<br>
                            Last Sold Date: {{ $mlsProperty->lastSoldDate() }}<br>
                            Last Sold Price: {{ $mlsProperty->lastSoldPrice() }}<br>
                            Tax Assessment: {{ $mlsProperty->taxAssessment() }}<br>
                            Tax Assessment Year: {{ $mlsProperty->taxAssessmentYear() }}<br>
                            Image: <img src="{{ $mlsProperty->imageUrl() }}"><br>
                        @else
                            Nothing found
                        @endif

                    </div>
                </div>
            </div>
        </div>
    @endif

</div>
@endsection
