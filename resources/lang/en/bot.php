<?php

return [
    'not_found' => 'We have not found such a property, try to write the address more accurately.',
    'phone_success' => "Thank you for choosing iBuerShop!",
    'estimate' => "Here is our estimate of your net proceeds.\nYou can earn from $:min to $:max\nFinal offer will be calculated after home inspection.",
    'closing_date' => "Estimated closing day is :month :day, :year",
    'last_sentence' => "If you agree click to \"Call me\"",
    'pre_answer' => 'Here is your house'
];
