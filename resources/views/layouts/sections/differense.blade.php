<section class="differense">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">What is our difference in working with iBuyershop</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="differense_wrap">
                    <div class="differense_wrap_item">
                        <h4>Self sale</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> List on the market for an average 50 days until closing</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Loss of your personal time searching for a buyer</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Hidden costs and other expenses</p>
                    </div>
                    <div class="differense_wrap_item middle">
                        <h4>iBuyershop</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> House for sale at a competitive price</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> Dedicated team of specialists</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> Minimum fee and more $ in your pocket</p>
                    </div>
                    <div class="differense_wrap_item">
                        <h4>Other Agents</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Sell instantly, but well below market value</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Agent is leading more 1 homes and your at the second terms of</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Home resold for a large profit</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
