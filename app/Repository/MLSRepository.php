<?php


namespace App\Repository;


use App\Property\PropertyInterface;
use App\Property\MLSMapper;
use DB;
use Log;
use MongoDB;

class MLSRepository implements PropertyRepositoryInterface
{
    public function getProperty(string $address, $city, $state, $zip = null): ?PropertyInterface
    {
        $filter = [];
        $filter['StreetAddress'] = strtoupper(preg_replace(['/\s+/', '/(^\s|\s$)/'], [' ', ''], $address));

        if (!empty($zip)) {
            $filter['PostalCode'] = $zip;
        } else {
            $filter['City'] = ucfirst($city);
            $filter['StateOrProvince'] = $state;
        }

        $mongoClient = new \MongoDB\Client("mongodb://mongo:27017", ['username' => 'root', 'password' => 'password', 'db' => 'MLS']);

        $listings = $mongoClient->MLS->listings
            ->find(
                $filter,
                [
                    'hint' => ['StreetAddress' => 1]
                ]
            )
          ;

        $listings = collect($listings->toArray());

        // No results
        if ($listings->isEmpty()) {
            return null;
        }

        if ($listings->count() > 1) {
            Log::debug('Multiple listings under 1 address', compact('address', 'city', 'state', 'zip'));
        }

        return new MLSMapper($listings->first()->getArrayCopy());
    }
}