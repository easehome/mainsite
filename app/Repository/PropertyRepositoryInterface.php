<?php


namespace App\Repository;


use App\Property\PropertyInterface;

interface PropertyRepositoryInterface
{
    public function getProperty(string $address, $city, $state, $zip = null): ?PropertyInterface;
}