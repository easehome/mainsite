<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sells extends Model
{
    protected $table = 'sells';
    protected $fillable = [
        'alias',
        'user_id',
        'address',
        'estimate',
        'imgs',
        'zip'
    ];
}
