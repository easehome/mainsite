<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function index() {
        return view('cabinet.settings.index');
    }

    public function change_password(ChangePasswordRequest $request) {
        $user = Auth::user();
        $user->password = Hash::make( $request->password );
        return back()->with([
            'success' => __('settings.change_password.success')
        ]);
    }
}
