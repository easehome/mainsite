<?php

namespace App\Http\Controllers;

use App\Model\Subscribers;
use App\Http\Requests\SubscriberRequest;
use Illuminate\Support\Facades\Log;
use App\Model\SubscribeType;


class SubscribeController extends Controller
{
    public function index(SubscriberRequest $request) {
        try {
            $this->subscribeToBase($request->email);
            $title="You subscribed successfull";
            $text="Your email will reciev news from us";
            $response = compact('title','text');
        }catch (\Exception $e) {
            Log::error('SUBSCRIBE', [$e->getMessage()]);
            $title="Something went wrong";
            $text="Try later to subscribe";
            $response = compact('title','text');
        }

        return view('pages.success', $response);
    }

    public function subscribeToBase($email) {
        return Subscribers::create([
            'type' =>  SubscribeType::where('name', 'All news')->value('id'),
            'email' => $email
        ]);
    }
}
