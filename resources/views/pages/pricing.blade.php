@extends('layouts.app')

@section('title', "Pricing | ".env('APP_NAME'))
@section('description', "Description")

@section('content')
    <section class="pricing">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Our Pricing</h2>
                        <p class="text">How our Fee is Formed</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pricing_tables">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-12 col-lg-5 col-md-6">
                    <div class="pricing_tables_item">
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>iBuyershop</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>7%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Average days to close transaction</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p class="light">Choose from 20-40 days</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--full">
                                <p>Transaction costs</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Average our service charge</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>7%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated real estate agent fees</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated seller concessions</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated home ownership and overlap</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Total</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>7%</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-lg-5 col-md-6">
                    <div class="pricing_tables_item">
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>iBuyershop</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>7-10%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Average days to close transaction</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p class="light">More than 50 days</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--full">
                                <p>Transaction costs</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Average our service charge</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated real estate agent fees</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>6.5%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated seller concessions</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>2.3%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Estimated home ownership and overlap</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>1%</p>
                            </div>
                        </div>
                        <div class="pricing_tables_item--row">
                            <div class="pricing_tables_item--left">
                                <p>Total</p>
                            </div>
                            <div class="pricing_tables_item--right">
                                <p>9.8%</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.sections.differense')

@endsection
