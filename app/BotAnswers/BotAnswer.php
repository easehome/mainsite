<?php


namespace App\BotAnswers;


interface BotAnswer
{
    public function addMessageText(string $text, array $buttons = [] );

    public function addMessageGallery(array $images, array $buttons = []);

    public function addMessageImage(string $img, array $buttons = [] );

    public function addQuickReplies(string $caption, string $target);
}
