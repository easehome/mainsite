@extends('layouts.app')

@section('content')
<section class="reset_password">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Reset Password</h2>
                    <p class="text">Enter you data to success</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4 offset-lg-4">
                <form action="{{ route('password.email') }}" method="POST" class="sign_form">
                    @csrf
                    <div class="form-group row">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Your Email">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group row mb-0">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
