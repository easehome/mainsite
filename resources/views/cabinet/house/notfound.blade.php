@extends('layouts.app')
@section('title', "Pending calculate estimate House | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="house_pending">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title" style="margin-bottom: 30px;">{{ __('houses.notfound.title') }}</h2>
                        <p class="text">{{ __('houses.notfound.text') }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 offset-md-4">
                    <form class="sign_form house_pending_form" method="POST" action="{{ route('getoffer') }}">
                        <legend><p>{{ __('houses.notfound.offer') }}</p></legend>
                        @csrf
                        <div class="form-group">
                            <input  type="text" class="form-control" name="address" value="{{ old('address') ?? $address }}" required="" placeholder="Address home or apartment">
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input  type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required="" placeholder="First name">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input  type="text" class="form-control" name="second_name" value="{{ old('second_name') }}" required="" placeholder="Second name">
                            @if ($errors->has('second_name'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('second_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required="" placeholder="Phone">
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-full">Request offer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
