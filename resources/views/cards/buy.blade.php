@extends('layouts.app')
@section('title', "Contacts | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="card_main">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="card_main_info">
                        <div class="card-carusel owl-carousel">
                            @foreach($house['imgs'] as $img)
                            <div class="card-carusel-item">
                                <img class="card-carusel-img" src="{{ $img }}" alt="">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card_info">
                        <h2>{{ $house['address'] }}</h2>
                        <div class="card_info_spec">
                            <div class="card_info_spec__info">
                                <p class="card_info_option">
                                    <span class="icon"><i class="flaticon-bed"></i></span>  {{ $house['bedrooms'] }} Bed
                                </p>
                                <p class="card_info_option">
                                    <span class="icon"><i class="flaticon-bathtub"></i></span> {{ $house['bathrooms'] }} Bath
                                </p>
                            </div>
                            <div class="card_info_spec__info">
                                <p class="card_info_option">{{ $house['sqr'] }} sqft</p>
                                <p class="card_info_option">$170 / Sq. Ft.</p>
                            </div>
                        </div>
                        <hr>
                        @if(isset($house['lastSoldPrice']))
                        <p class="card_info_option">
                            Last Sold Price: ${{ number_format($house['lastSoldPrice']) }}
                        </p>
                        @endif
                        <h3>Estimate: <span>${{ number_format($house['zestimate']) }}</span></h3>
                        @if(count($house['zestimatePricaRange']) > 0)
                        <p class="card_info_option_min">Estimate range: ${{number_format($house['zestimatePricaRange']['low'])}} - ${{number_format($house['zestimatePricaRange']['high'])}}</p>
                        @endif
                        <div class="card_info_offer">
                            <p class="card_info_offer--title">Want sell? We can help you make it easer.</p>
                            <a href="#" class="btn">Click EaseSell</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card_about">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="card_about_wrapper">

                        <div class="card_about_facts">
                            <h2>Facts and Features</h2>
                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>

                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>

                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>

                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>

                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>

                            <div class="card_about_facts_item">
                                <i class="flaticon-bathtub"></i>
                                <div class="card_about_facts_item--text">
                                    <p class="card_about_facts_item--title">Type</p>
                                    <p class="card_about_facts_item--text">Single Family</p>
                                </div>
                            </div>
                        </div>
                        <!-- /facts -->

                        <div class="card_map">
                            <div id="map"></div>
                        </div>

                        @if(isset($house['descr']))
                        <div class="card_about">
                            <p class="card_about--title">About This Home</p>
                            <p class="card_about--descr">{{ $house['descr'] }}</p>
                        </div>
                        @endif
                        <div class="card_history">
                            <p class="card_about--title">Property History for 1184 SW Ocean Dr</p>
                            <div class="card_history_table">
                                <div class="card_history_table--row caption">
                                    <div class="card_history_table--1">Date</div>
                                    <div class="card_history_table--2">Event</div>
                                    <div class="card_history_table--3">Price</div>
                                </div>
                                <div class="card_history_table--row">
                                    <div class="card_history_table--1">Nov 22, 2002</div>
                                    <div class="card_history_table--2">Sold (Public Records)</div>
                                    <div class="card_history_table--3">$130,000</div>
                                </div>
                                <div class="card_history_table--row">
                                    <div class="card_history_table--1">Apr 1, 2019</div>
                                    <div class="card_history_table--2">Sold (Public Records)</div>
                                    <div class="card_history_table--3">$230,000</div>
                                </div>
                            </div>
                        </div>

                        <div class="card_chart_price">
                            <canvas id="chart1"></canvas>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- scripts -->
<script>
    function initMap() {
        var mapSign = {lat: {{ $house['lat'] }}, lng: {{ $house['lng'] }}};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: mapSign
        });
        var marker = new google.maps.Marker({
            position: mapSign,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ $google_key }}&callback=initMap">
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>
<script>
    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    function randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }

    function randomBar(date, lastClose) {
        var open = randomNumber(lastClose * 0.95, lastClose * 1.05).toFixed(2);
        var close = randomNumber(open * 0.95, open * 1.05).toFixed(2);
        return {
            t: date.valueOf(),
            y: close
        };
    }

    var dateFormat = 'MMMM DD YYYY';
    var date = moment('April 01 2017', dateFormat);
    var data = [randomBar(date, 30)];
    while (data.length < 60) {
        date = date.clone().add(1, 'd');
        if (date.isoWeekday() <= 5) {
            data.push(randomBar(date, data[data.length - 1].y));
        }
    }
    var ctx = document.getElementById('chart1').getContext('2d');

    var color = Chart.helpers.color;
    var cfg = {
        type: 'bar',
        data: {
            datasets: [{
                label: 'CHRT - Chart.js Corporation',
                backgroundColor: color('#0099ff').alpha(0.5).rgbString(),
                borderColor: '#0099ff',
                data: data,
                type: 'bar',
                pointRadius: 0,
                fill: false,
                lineTension: 0,
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'series',
                    ticks: {
                        source: 'data',
                        autoSkip: true
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Closing price ($)'
                    }
                }]
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                callbacks: {
                    label: function(tooltipItem, myData) {
                        var label = myData.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ': ';
                        }
                        label += parseFloat(tooltipItem.value).toFixed(2);
                        return label;
                    }
                }
            }
        }
    };

    var chart = new Chart(ctx, cfg);
</script>
@endsection
