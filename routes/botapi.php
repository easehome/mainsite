<?php

use Illuminate\Http\Request;


Route::post('property', 'BotApiController@property');
Route::post('order', 'BotApiController@order');
Route::match(['get', 'post'], '/manychat', 'BotApiController@manychat');
Route::match(['get', 'post'], '/manychat_order', 'BotApiController@manychat_order');
