<?php


namespace App\Order;


use App\Model\Orders;
use App\Model\Sells;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class OrderHelper
{
    public static function registerUser($post)
    {
        $user = self::createUser($post);

        event(new Registered( $user ));

        self::guard()->login($user);

        return $user;
    }

    protected static function createUser($data) {
        $password = Hash::make( Str::random(10) );

        return User::create([
            'first_name' => $data['first_name'],
            'second_name' => $data['second_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => $password,
        ]);
    }


    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    public static function guard()
    {
        return Auth::guard();
    }

    public static function createOrder(User $user, string $address): Orders
    {
        return Orders::create([
            'first_name' => $user->first_name,
            'second_name' => $user->second_name,
            'phone' => $user->phone,
            'email' => $user->email,
            'address' => $address
        ]);
    }

    public static function createSell(User $user, string $address)
    {
        $url = Str::slug($address);
        $addressSession = Session::get('address');
        Sells::firstOrCreate([
            'alias' => $url,
            'user_id' => $user->id,
            'address' => $address,
            'zip' => $addressSession['zip'],
        ]);

        return $url;
    }
}