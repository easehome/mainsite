<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Sells;
use App\Repository\RepositoryFactory;
use App\Traits\EstimateFormat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    use EstimateFormat;

    public function index(Request $request) {
        $user = Auth::user();
        $get_houses = Sells::where('user_id',$user->id)->get();
        $houses = [];
        foreach($get_houses as $house) {
            $repository = RepositoryFactory::getRepository(RepositoryFactory::ZILLOW);
            $address = explode(',',$house->address)[0];
            $property = $repository->getProperty($address, null, null, $house->zip);

            if (! $property) {
                $houses[] = [
                    'address' => $address,
                ];
                continue;
            }

            //dd($property);
            $tempHouse = [
                'address' => $address,
                'lat' => (float) $property->latitude(),
                'lng' => (float) $property->longitude(),
                'bathrooms' => (float) $property->bathroomsCount(),
                'sqr' => (float) $property->areaSize(),
                'bedrooms' => (float) $property->bedroomsCount(),
                'zestimate' => $property->estimatedPrice(),
                'imgs' => $property->images(),
                'alias' => $house->alias
            ];

            if( $estimatedPrice = $property->estimatedPrice() ) $tempHouse['prices'] = $this->formatEstimate($estimatedPrice);

            $houses[] = $tempHouse;
        }
        return view('cabinet.index',compact('houses'));
    }
}
