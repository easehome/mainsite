<footer>
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-12">
                    <div class="fw-logo footer-widget">
                        <p class="flogo"><span class="color">iBuyer</span>shop</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 col-sm-5 col-12">
                    <div class="fw-address footer-widget">
                        <h4 class="title">Contact Us</h4>
                        <ul class="address">
                            <li>
                                <span class="icon"><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i></span>
                                {{ config('contacs.address') }}
                            </li>
                            <li>
                                <span class="icon"><i class="flaticon-envelope"></i></span>
                                <a href="mailto:{{ config('contacs.mail.info') }}">{{ config('contacs.mail.info') }}</a></li>
                            <li>
                                <span class="icon"><i class="flaticon-phone-receiver"></i></span>
                                <a href="tel:{{ config('contacs.phone.full') }}">{{ config('contacs.phone.format') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-3 offset-md-1 col-sm-4 col-12">
                    <div class="fw-links footer-widget">
                        <h4 class="title">Quick Links</h4>
                        <ul class="links">
                            <li><a href="{{ route('home') }}">Sell</a></li>
                            <li><a href="{{ route('buy') }}">Buy</a></li>
                            <li><a href="{{ route('howitworks') }}">How it works</a></li>
                            <li><a href="{{ route('pricing') }}">Pricing</a></li>
                            <li><a href="{{ route('about') }}">About us</a></li>
                            <li><a href="{{ route('investors')  }}">Investors</a></li>
                            <li><a href="{{ route('vendors')  }}">Vendors</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8 col-12">
                    <div class="fw-subscribe footer-widget">
                        <h4 class="title">Stay in touch</h4>
                        <div class="fw-subscribe-input-box">
                            <form method="POST" action="{{ route('email_subscribe') }}">
                                @csrf
                                <input name="email" type="email" placeholder="Enter Email Address" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                                @endif
                                <button type="submit"><i class="fab fa-telegram-plane"></i></button>
                            </form>
                        </div>
                        <h4 class="title">Folllow</h4>
                        <ul class="social">
                            @foreach(config('contacs.social') as $social)
                            <li><a href="{{ $social['link'] }}" target="_blank"><i class="fab {{ $social['icon'] }}"></i></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                    <p class="copyright">© iBuyershop 2019. AL Rights Reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-4 col-12">
                    <p class="top">
                        <a href="#top" class="link smoothscroll">Back on Top <span class="icon"><i class="fas fa-chevron-up"></i></span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
