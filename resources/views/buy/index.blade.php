@extends('layouts.app')
@section('title', "Buy | ".env('APP_NAME'))
@section('description', "Description")

@section('content')
<section class="buy_map">
    <div class="buy_map_content">

        {{--
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8">
                    <form action="{{ route('buy_search') }}" class="form-gorizontal form-buy">
                        <div class="form-input-box">
                            <input type="text" name="address" id="" placeholder="City, Adress, ZIP">
                        </div>
                        <div class="form-input-box btn-box">
                            <button type="submit" class="btn">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        --}}
        <div class="buy_map_wrapper">
            <div class="buy_map_list">
                <h2>Houses in this area</h2>

                @foreach($houses as $house)
                <div class="buy_map_list--item">
                    <a href="{{ $house['link'] }}">
                        <div class="single-feature">
                            <div class="img">
                                <img src="{{ $house['img'] }}" alt="">
                            </div>
                            <div class="content">
                                <h4 class="price">${{ $house['price'] }}</h4>
                                <p class="info">{{ $house['address'] }}</p>
                                <ul class="info-list">
                                    <li><span class="icon"><i class="flaticon-bed"></i></span> 1 Bed</li>
                                    <li><span class="icon"><i class="flaticon-bathtub"></i></span> 1 Bath</li>
                                </ul>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="buy_map_places">
                <div id="map"></div>
            </div>
        </div>

    </div>
</section>
<script>document.querySelector('body').classList.add('buy_fixed');</script>

<script>
    function initMap() {
        var mapSign = {lat: 25.795361, lng: -80.290111};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: mapSign
        });
        var marker = new google.maps.Marker({
            position: mapSign,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ $google_key }}&callback=initMap">
</script>

@endsection
