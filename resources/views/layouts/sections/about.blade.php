<section class="invest_about">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10">
                <div class="section-title">
                    <h2 class="title">About company</h2>
                    <p class="text">The idea of iBuyershop is based on several well-established principles of the
                        two-trillion dollar real estate market in the United States.</p>
                    <p class="text">Many homeowners in the United States find themselves in situations where they need to sell their most valuable asset, their homes (5% to 10% of all sellers are distressed). The average closing time of the transaction through the agent is 3 to 6 months and carries a lot of overlays and hassles.</p>
                    <p class="text">Through big data mining we can identify and target distressed sellers who have not yet put their properties on the market. The historical data analysis of distressed sellers allows our AI to point out factors which have led property owners to sell below market price. Those factors are the key to generating leads in the future.</p>
                    <p class="text">Our algorithms which work at the expense of AI neural networks, allow us to calculate the investment potential of their assets and close deals as soon as possible. The solution to the problem of distressed sellers allows us to buy out assets at low end market value. After the purchase of the property all the needed renovations are outsourced. Through the mining of data we know what criteria will most significantly increase the value of the asset. After the needed labor is done, the property re-sells and generates income through the increase in the value, or rent for passive income. The decision is made based on the investment potential of real estate for maximum capitalization of the company.</p>
                </div>
            </div>
        </div>
    </div>
</section>
