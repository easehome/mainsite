<?php

return [
    'notfound' => [
        'title' => 'Home is not yet in our database',
        'text'  => 'Would you like to add a house and get an estimated cost',
        'offer' => 'Leave your contact details and we will calculate the estimated cost'
    ],
];
