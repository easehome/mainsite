<section class="cabinet_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="cabinet_menu_nav">
                    <ul>
                        <li><a href="{{ route('cabinet.home') }}" class="cabinet_menu_nav--link">Favorits</a></li>
                        <li><a href="{{ route('cabinet.settings') }}" class="cabinet_menu_nav--link">Settings</a></li>
                        <li><a href="{{ route('sign_out') }}" class="cabinet_menu_nav--link">Sign Out</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
