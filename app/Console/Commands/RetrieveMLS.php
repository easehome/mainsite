<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RetrieveMLS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mls:retrieve {limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);

        $config = \PHRETS\Configuration::load([
            'login_url' => config('services.mls.login_url'),
            'username' => config('services.mls.username'),
            'password' => config('services.mls.password'),
            'user_agent' => 'UserAgent/1.0',
//        'user_agent_password' => 'user_agent_password_here',
            'rets_version' => '1.8',
//        'http_authentication' => 'basic',
        ]);

        $retsClient = new \PHRETS\Session($config);

        $mongoClient = new \MongoDB\Client("mongodb://mongo:27017", ['username' => 'root', 'password' => 'password', 'db' => 'MLS']);

        $connect = $retsClient->Login();

        // $system = $retsClient->GetSystemMetadata();

        $limit = $this->argument('limit') ?? 1000;
        $offset = 0;

        $getResults = function($offset) use (&$retsClient, $limit) {

            return $retsClient->Search(
                'Property',
                'Listing',
                '(LastChangeTimestamp=1950-01-01T00:00:00+)',
                [
                    'Limit' => $limit,
                    'Offset' => $offset,
                    'Count' => 0
                ]);
        };

        while(($results = $getResults($offset)) && $results->count() > 0) {

            $mongoClient->MLS->listings->insertMany($results->toArray());
            $this->info("Written {$results->count()} entries.");

            $offset += $limit;
            /* free memory */
            $results = null;
        }
        // dump($results->getTotalResultsCount());

        $this->info("Time: " . (microtime(true) - $start));
        $this->info("Memory: " . memory_get_peak_usage());
    }
}
