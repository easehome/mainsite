@extends('layouts.app')
@section('title', env('APP_NAME'))
@section('description', "Description")
@section('content')
<section class="header" id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-12">
                <div class="hero2-content">
                    <h2 class="title">Sell your home easy with iBuyershop</h2>
                </div>
            </div>
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 164.9"><defs><style>.cls-1{fill:#09f;}.cls-2{fill:#02d8ff;}.cls-3{fill:#fff;}</style></defs><title>Ресурс 1</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M584.06,34.58C568,69.52,511.7,99.6,459,103.69L0,139.23v6.41l468.14-36.25c52.14-4,107.76-33.79,123.69-68.34L600,23.34V0Z"/><path class="cls-2" d="M468.14,109.39,0,145.64v6.68l473-36.63c52.73-4.09,109-34.17,125.09-69.11L600,42.36v-19l-8.17,17.71C575.9,75.6,520.28,105.35,468.14,109.39Z"/><path class="cls-3" d="M473,115.69,0,152.32V164.9H600V42.36l-1.94,4.22C582,81.52,525.7,111.6,473,115.69Z"/></g></g></svg>
</section>
<section class="search2-area">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                <form action="{{ route('correct_address') }}" method="POST" class="js-search-form">
                    @csrf
                    <div class="search2-full-box">
                        <div class="search2-all-box">
                            <div class="search2-input-box">
                                <input name="address" class="js-address-autocomplete" onFocus="geolocate()" onkeypress="return event.keyCode == 13 ? false : true;" type="text" placeholder="Address, Zip Code">
                            </div>
                            <div class="search2-input-box btn-box">
                                <button type="submit" class="btn">Make me offer</button>
                            </div>
                        </div>
                        <a href="{{ route('home') }}" ><p class="see-advance">Easy sell</p></a>
                        <a href="{{ route('buy') }}" ><p class="see-advance">Easy buy</p></a>
                        <a href="{{ route('howitworks') }}" ><p class="see-advance">Help you sell</p></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="diffrent-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box wow fadeInUp">
                    <div class="img">
                        <img src="img/bestof_1.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">Prepare your home for sale</h4>
                        <p class="text">Don’t bother cleaning, because you won’t have to show your home</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box wow fadeInUp" data-wow-delay=".3s">
                    <div class="img">
                        <img src="img/bestof_2.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">You decide by which date to close</h4>
                        <p class="text">Pick a closing date that works with your plans.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box wow fadeInUp"  data-wow-delay=".6s">
                    <div class="img">
                        <img src="img/bestof_3.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">Save your personal time on showings</h4>
                        <p class="text">Our service will facilitate the sale or sale of your home</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about2-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Easy step to sell your property</h2>
                </div>
            </div>
        </div>
        <div class="about2-area-steps-wrap">
            <div class="row  justify-content-center">
                <div class="col-xl-3 col-md-4 col-12">
                    <div class="about2-info" data-wow-delay="">
                        <p class="about2__number">1</p>
                        <h2 class="title">​​Enter your home address and get market estimate price with our recomendation</h2>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                    <div class="about2-info " data-wow-delay=".3s">
                        <p class="about2__number">2</p>
                        <h2 class="title">After a few days we send you cash offer and you make choise: accept offer or work with an agent.</h2>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                    <div class="about2-info " data-wow-delay=".6s">
                        <p class="about2__number">3</p>
                        <h2 class="title">Pick a closing date and take paid within few days</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="how">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">How we make estimate offer</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box">
                    <div class="img">
                        <img src="img/how_1.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">Use Unique Tehnology</h4>
                        <p class="text">We use online tools to make you smarter calculate estimate price.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box" data-wow-delay=".3s">
                    <div class="img">
                        <img src="img/how_2.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">Neighborhood comparisons</h4>
                        <p class="text">We evaluate homes recently sold near you and evaluate based on the options of your home.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="single-diffrent-box"  data-wow-delay=".6s">
                    <div class="img">
                        <img src="img/how_3.svg" alt="">
                    </div>
                    <div class="content">
                        <h4 class="name">Use BigData sells</h4>
                        <p class="text">We analyze big sales data for the last years and provide the most relevant price for today</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="states">
    <div class="container">
        <div class="states_wrap">
            <h2 class="title">Easy to buy and sell homes in Florida!</h2>
            <h4 class="text">Coming soon to your state too...</h4>
        </div>
    </div>
</section>

<section class="differense">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">What is our difference in working with iBuyershop</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="differense_wrap">
                    <div class="differense_wrap_item">
                        <h4>Self sale</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> List on the market for an average 50 days until closing</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Loss of your personal time searching for a buyer</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Hidden costs and other expenses</p>
                    </div>
                    <div class="differense_wrap_item middle">
                        <h4>iBuyershop</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> House for sale at a competitive price</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> Dedicated team of specialists</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-thumbs-up"></i> Minimum fee and more $ in your pocket</p>
                    </div>
                    <div class="differense_wrap_item">
                        <h4>Other Agents</h4>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Sell instantly, but well below market value</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Agent is leading more 1 homes and your at the second terms of</p>
                        <p class="differense_wrap_item--text"><i class="fas fa-minus"></i> Home resold for a large profit</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="faq">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Frequently asked questions</h2>
                </div>
            </div>
            <div class="col-12 col-md-10 offset-md-1">
                <div class="multiline">
                    @foreach($faqs as $k=>$faq)
                    <div class="multiline_item">
                        <button
                            class="multiline_item_button collapsed"
                            type="button"
                            data-toggle="collapse"
                            data-target="#question_{{ $k }}"
                            aria-expanded="true"
                            aria-controls="collapseOne">
                            {{ $faq['q'] }}
                            <i class="fas fa-chevron-up"></i>
                        </button>
                        <div id="question_{{ $k }}" class="multiline_item_description collapse">
                            <div class="multiline_item_description__wrapper">
                                <p>{{ $faq['a'] }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@if(count($houses) > 0)
<section class="funded-transaction-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-12">
                <div class="section-title">
                    <h2 class="title">Houses for sale</h2>
                    <p class="text">The most popular houses in our database</p>
                </div>
            </div>
        </div>
        <div class="funded-carousel owl-carousel">
            @foreach($houses as $house)
            <div class="single-funded">
                <div class="img" style="background-image:url({{ $house['img'] }})">

                    <span class="type">{{ $house['mark'] }}</span>
                </div>
                <div class="content">
                    <a href="{{ $house['url'] }}" class="name">{{ $house['address'] }}</a>
                    <p class="adderss"></p>
                    <h2 class="price">${{ $house['price'] }}</h2>
                    <p class="status">Funding Requested</p>
                </div>
                <div class="down-info">
                    <div class="di-box">
                        <h4 class="unit">13%</h4>
                        <p class="name">APR</p>
                    </div>
                    <div class="di-box">
                        <h4 class="unit">58%</h4>
                        <p class="name">LTV</p>
                    </div>
                    <div class="di-box">
                        <h4 class="unit">12mth</h4>
                        <p class="name">LOAN TERM</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif

<section class="callback-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-12">
                <div class="callback-title">
                    <h4 class="intro">How Can Help?</h4>
                    <h2 class="title">get offer now - write your adress</h2>
                </div>
            </div>
        </div>
        <form class="callback-input-area" action="tralala">
            <div class="cib-email calback-input-box">
                <input name="address" type="text" placeholder="Address, Zip Code" required>
            </div>
            <div class="cib-phone calback-input-box">
                <input name="email" type="email" placeholder="Your Email" required>
            </div>
            <div class="cib-submit calback-input-box">
                <button type="submit">Make me offer</button>
            </div>
        </form>
    </div>
</section>
@push('scripts')
<script src="{{ asset('js/suggestions.js') }}?{{ time() }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&libraries=places&callback=initAutocomplete&language=en"
        async defer></script>
<script>
    $('.callback-input-area').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "{{ route('callback') }}",
            type:'post',
            data: $(this).serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                form.find('[type=submit]').prop('disabled',true);
            },
            success: function(response) {
                if(response.response) showModal(1,response.response);
                if(response.error) showModal(0,response.error);
            },
            error: function() {
                console.log('some error')
            },
            complete: function() {
                form.find('[type=submit]').prop('disabled',false);
                form.trigger('reset');
            }
        })
    })
</script>
@endpush
@endsection
