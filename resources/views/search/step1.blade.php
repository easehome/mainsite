@extends('layouts.app')
@section('title', "Home | ".env('APP_NAME'))
@section('description', "description")
@section('content')
    <section class="step1">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Its your address - {{ $address['street'] }} ?</h2>
                    </div>
                </div>
                <div class="col-12">
                    <div id="map"></div>
                </div>
                <div class="col-12 text-center">
                    <a class="button button-reverce outline home1-link link" href="{{ route('home') }}">No, checnge it</a>
                    <form action="{{ route('offer_me') }}" method="POST" style="display: inline-block;">
                        @csrf
                        <input type="hidden" name="confirm" value="1">
                        <input type="hidden" name="address" value="{{ $address['street'] }}">
                        <button class="button  home1-link link hvr-bs" type="submit">Yes</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script>
        function initMap() {
            var mapSign = {lat: {{ $address['lat'] }}, lng: {{ $address['lng'] }} };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: mapSign
            });
            var marker = new google.maps.Marker({
                position: mapSign,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ $google_key }}&callback=initMap">
    </script>
@endsection
