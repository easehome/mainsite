@extends('layouts.app')

@section('title', "Contacts | ".env('APP_NAME'))
@section('description', "description")

@section('content')
    <section class="contact_header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_wrapper">
                        <h2>You have some questions?</h2>
                        <h2 class="title">Contact us!</h2>
                        <p class="text"><a href="mailto:{{ config('contacs.mail.info') }}">{{ config('contacs.mail.info') }}</a></p>
                        <p class="text"><a href="tel:{{ config('contacs.phone.full') }}">{{ config('contacs.phone.format') }}</a></p>
                        <p class="text">(6am-7pm ET)</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="offices">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">Our Offices</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach(config('contacs.offices') as $office => $info)
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="single-feature">
                        <div class="content">
                            <h4 class="price">{{ $office }}</h4>
                            <p class="info">{{ $info['address'] }}</p>
                            <ul class="info-list">
                                <li><span class="icon"><i class="fas fa-envelope"></i></span> {{ $info['email'] }}</li>
                                <li><span class="icon"><i class="fas fa-mobile-alt"></i></span> {{ $info['phone'] }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="line"></div>
                </div>
            </div>
            <div class="row">
                @foreach( config('contacs.emails') as $type => $email)
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="office_item">
                            <h4>For {{ $type }}</h4>
                            <p class="text"><a href="mailto:{{ $email }}">{{ $email }}</a></p>
                        </div>
                    </div>
                @endforeach
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="office_item">
                        <h4>Stay with us in:</h4>
                        <ul class="social">
                            @foreach(config('contacs.social') as $social)
                                <li><a href="{{ $social['link'] }}" target="_blank"><i class="fab {{ $social['icon'] }}"></i></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
