<header class="header-2" >
    <div class="@if(in_array( Request::path(), ['buy'] )) container-fluid @else container @endif">
        <div class="row">
            <div class="col-lg-2 col-12">
                <div class="logo">
                    <a class="logo-link menu-item" href="{{ route('home') }}"><span class="color">iBuyer</span>shop</a>
                    <a class="logo_phone menu-item" href="tel:{{config('contacs.phone.full')}}"><span class="flaticon-phone-receiver"></span> {{config('contacs.phone.format')}}</a>
                </div>
            </div>
            <div class="col-lg-10 col-12">
                <div class="menu">
                    <nav id="mobile_menu_active">
                        <ul>
                            <li><a href="{{ route('home') }}" class="menu-item menu_list_a">Sell</a></li>
                            <li><a href="{{ route('buy') }}" class="menu-item menu_list_a">Buy</a></li>
                            <li><a href="{{ route('howitworks') }}" class="menu-item menu_list_a">How it works</a></li>
                            <li><a href="{{ route('pricing') }}" class="menu-item menu_list_a">Pricing</a></li>
                            <li><a href="{{ route('about') }}" class="menu-item menu_list_a">About us</a></li>
                            <li><a class="" href="#">
                                    <svg style="width: 5px" version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 85 301" style="enable-background:new 0 0 85 301;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0{fill:#223c55;}
                                            </style>
                                        <g>
                                            <g>
                                                <circle class="st0" cx="42.5" cy="42.5" r="32"/>
                                            </g>
                                            <g>
                                                <circle class="st0" cx="42.5" cy="150.5" r="32"/>
                                            </g>
                                            <g>
                                                <circle class="st0" cx="42.5" cy="258.5" r="32"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <ul class="drop">
                                    <li><a href="{{ route('investors') }}" class="menu-item menu_list_a">Investors</a></li>
                                    <li><a href="{{ route('vendors') }}" class="menu-item menu_list_a">Vendors</a></li>
                                    <li><a href="{{ route('contact') }}" class="menu-item menu_list_a">Contact</a></li>
                                    <!-- <li><a href="#" class="menu-item menu_list_a">FAQ</a></li> -->
                                </ul>
                            </li>
                        </ul>
                        @guest
                            <a class="js-sign-item button button-reverce outline home1-link link" href="{{ route('login') }}">Sign in</a>
                            <a class="js-sign-item button  home1-link link hvr-bs" href="{{ route('register') }}">Sign up</a>
                        @else
                            <div class="cabinet_button js-sign-item">
                                <i class="fas fa-user"></i>
                                <p>{{ Auth::user()->email }}</p>
                                <ul class="drop">
                                    <li><a href="{{ route('cabinet.home') }}" class="menu-item menu_list_a">Favorits</a></li>
                                    <li><a href="{{ route('cabinet.settings') }}" class="menu-item menu_list_a">Settings</a></li>
                                    <li><a href="{{ route('sign_out') }}" class="menu-item menu_list_a">Exit</a></li>
                                </ul>
                            </div>
                        @endguest
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
