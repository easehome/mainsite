@extends('layouts.app')

@section('title', "404 Page not found | ".env('APP_NAME'))
@section('description', "description")

@section('content')
    <section class="error">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h2 class="title">Page not found</h2>
                        <p class="text" style="margin-bottom: 20px;">We are sorry, but there is no such page.</p>
                        <a href="{{ route('home') }}" style="color:#fff;" class="home1-link link hvr-bs">Go to main page</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
