<?php


namespace App\Traits;


trait EstimateFormat
{
    /**
     * @param int $estimatedPrice
     * @return array number_formats
     */
    public function formatEstimate(int $estimatedPrice): array
    {
        $minPercent = config('prices.percent.min');
        $maxPercent = config('prices.percent.max');

        // obfuscate estimate
        $newEstimateMin = (int) ($estimatedPrice * ((100 - $minPercent) / 100));
        $newEstimateMax = (int) ($estimatedPrice * ((100 - $maxPercent) / 100));

        //format to American style
        $newEstimateMin = number_format($newEstimateMin);
        $newEstimateMax = number_format($newEstimateMax);

        return [
            'min' => $newEstimateMin,
            'max' => $newEstimateMax
        ];
    }
}
