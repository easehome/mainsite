<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Repository\RepositoryFactory;

Auth::routes(['verify' => true]);
Route::get('/sign_out', 'Auth\LoginController@getLogout')->name('sign_out');

//pages
Route::get('/', 'HomeController@index')->name('home');
Route::get('/how-it-works', function() { return view('pages.howitworks'); })->name('howitworks');
Route::get('/pricing', function() { return view('pages.pricing'); })->name('pricing');
Route::get('/about', function() { return view('pages.about'); })->name('about');
Route::get('/investors', function() { return view('pages.investors'); })->name('investors');
Route::get('/contact', function() { return view('pages.contact'); })->name('contact');

Route::get('/card', function() { return view('cards.buy'); });

//vendors
Route::get('/vendors', 'VendorsController@index')->name('vendors');
Route::post('/vendors', 'VendorsController@getoffer')->name('vendors_order');

//buy
Route::get('/buy', 'BuyController@index')->name('buy');
Route::post('/buy', 'BuyController@search')->name('buy_search');

//getoffers
Route::post('/getoffer', 'OrderController@index')->name('getoffer');
Route::post('/_callback', 'OrderController@callback')->name('callback');
Route::post('/investors', 'OrderController@investors')->name('investors_post');
Route::post('/email_subscribe', 'SubscribeController@index')->name('email_subscribe');

//sell steps
//step1
Route::post('/correct-address', 'SearchController@step1')->name('correct_address');
Route::get('/correct-address', 'SearchController@step1show')->name('correct_address_show');

//step2
Route::post('/offer-me', 'SearchController@step2')->name('offer_me');
Route::get('/offer-me', 'SearchController@step2show')->name('offer_me_show');


//other
Route::get('/search', 'SearchController@showSearch');
Route::post('/search', 'SearchController@execSearch')->name('search-results');

//cabinet
Route::group([
    'middleware' => ['auth'],
    'namespace' => 'Cabinet',
    'prefix' => 'myaccount',
    'as' => 'cabinet.'
], function() {
    Route::get('/', 'DashboardController@index')->name('home');
    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::post('/settings/change_password', 'SettingsController@change_password')->name('change_password');

    Route::get('/{house}', 'HouseController@index')->name('house_id');
});
