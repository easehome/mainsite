<?php


namespace App\Repository;


class RepositoryFactory
{
    const ZILLOW = 'zillow';
    const MLS = 'mls';

    public static function getRepository($repository = self::ZILLOW) : PropertyRepositoryInterface
    {
        switch ($repository) {
            case self::ZILLOW:
                return new ZillowRepository(config('services.zillow.zws-id'));
                break;
            case self::MLS:
                return new MLSRepository();
                break;
        }
    }
}