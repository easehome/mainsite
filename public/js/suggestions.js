var autocomplete;

var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

/* Init form */
(function(form) {
  if (!form) {
    return;
  }

  for (var fieldName in componentForm) {
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", fieldName);
    form.appendChild(hiddenField);
  }

  form.addEventListener('submit', function(e) {
    if (form.querySelector('[name=street_number]').value === '') {
      alert("Please choose a valid home address from the drowdown list.");
      form.querySelector('.js-address-autocomplete').focus();
      e.preventDefault();
    }
  });
})(document.querySelector('.js-search-form'));

function initAutocomplete() {
  var inputs =  document.getElementsByClassName('js-address-autocomplete');

  for(input of inputs) {
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
      input, {
        types: ['address'],
        ComponentRestrictions: {'country': 'us'},
      });

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    autocomplete.setFields(['address_component']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  var form = document.querySelector('.js-search-form');

  if (!place.address_components) {
    form.querySelector('[name=street_number]').value = '';
    return;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      form.querySelector('[name='+addressType+']').setAttribute("value", val);
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
        {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
