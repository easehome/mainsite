@extends('layouts.app')

@section('title', "How it works with | ".env('APP_NAME'))
@section('description', "Description")

@section('content')
    <section class="howheader">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>How it works<br>with iBuyershop</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="howsteps">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="section-title tal">
                        <h2 class="title">Easy sell</h2>
                        <p class="text">Easy steps to sell your house or apartment</p>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="howsteps_wrapper">
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">1</div>
                            </div>
                            <div class="step_description">
                                <p class="text">Find your home, check out our estimations, and request the offer</p>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">2</div>
                            </div>
                            <div class="step_description">
                                <p class="text">Our experts prepare the offer favorable to you</p>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">3</div>
                            </div>
                            <div class="step_description">
                                <p class="text">You are satisfied and we prepare the property for shows. If you need to restore, we will arrange a solution. We make out the schedule of shows.</p>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">4</div>
                            </div>
                            <div class="step_description">
                                <p class="text">You without stress are looking for a new home or doing what you love, and we sell your house.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="section-title tal">
                        <h2 class="title">Easy Buy</h2>
                        <p class="text">Easy steps to buy your house or apartment</p>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="howsteps_wrapper">
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">1</div>
                            </div>
                            <div class="step_description">
                                <p class="text">Choose the house you are most interested in and sign up for a visit</p>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">2</div>
                            </div>
                            <div class="step_description">
                                <p class="text">Have you made your choice? we will help you to arrange everything quickly and easily</p>
                            </div>
                        </div>
                        <div class="step">
                            <div class="step_number">
                                <img src="img/forstep.png" alt="" class="step_number--back">
                                <div class="step_number--number">3</div>
                            </div>
                            <div class="step_description">
                                <p class="text">We sign the papers and you go to the new house</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
