<?php


namespace App\Property;
use SimpleXMLElement;
use Arr;

class ZillowMapper implements PropertyInterface, \JsonSerializable
{
    private $baseFields;
    private $extraFields;

    public function __construct(SimpleXMLElement $baseFields, ?SimpleXMLElement $extraFields)
    {
        $this->baseFields = $baseFields;
        $this->extraFields = $extraFields;
    }

    public function latitude(): ?float
    {
        return $this->field($this->baseFields, '//address/latitude');
    }

    public function longitude(): ?float
    {
        return $this->field($this->baseFields, '//address/longitude');
    }

    public function yearBuilt(): ?int
    {
        return isset($this->baseFields->yearBuilt) ? (int) $this->baseFields->yearBuilt : null;
    }

    public function areaSize(): ?int
    {
        return isset($this->baseFields->finishedSqFt) ? (int) $this->baseFields->finishedSqFt : null;
    }

    public function yardSize(): ?int
    {
        return isset($this->baseFields->lotSizeSqFt) ? (int) $this->baseFields->lotSizeSqFt : null;
    }

    public function estimatedPrice(): ?int
    {
        return $this->field($this->baseFields, '//zestimate/amount');
    }

    public function estimatedPriceRangeMin(): ?int
    {
        return $this->field($this->baseFields, '//zestimate/valuationRange/low');
    }

    public function estimatedPriceRangeMax(): ?int
    {
        return $this->field($this->baseFields, '//zestimate/valuationRange/high');
    }

    public function bathroomsCount(): ?int
    {
        return isset($this->baseFields->bathrooms) ? (int) $this->baseFields->bathrooms : null;
    }

    public function bedroomsCount(): ?int
    {
        return isset($this->baseFields->bedrooms) ? (int) $this->baseFields->bedrooms : null;
    }

    public function lastSoldPrice(): ?int
    {
        return isset($this->baseFields->lastSoldPrice) ? (int) $this->baseFields->lastSoldPrice : null;
    }

    public function lastSoldDate(): ?string
    {
        return isset($this->baseFields->lastSoldDate) ? (string) $this->baseFields->lastSoldDate : null;
    }

    public function taxAssessment(): ?float
    {
        return isset($this->baseFields->taxAssessment) ? (float) $this->baseFields->taxAssessment : null;
    }

    public function taxAssessmentYear(): ?int
    {
        return isset($this->baseFields->taxAssessmentYear) ? (int) $this->baseFields->taxAssessmentYear : null;
    }

    public function imageUrl(): ?string
    {
        return $this->field($this->extraFields, '//image/url');
    }

    public function images(): ?array
    {
        if(!isset($this->extraFields->images)) return [];
        $imagesArray = (array) $this->extraFields->images->image->url;
        return $imagesArray;
    }

    public function yearUpdated(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/yearUpdated');
    }

    public function numFloors(): ?int
    {
        return $this->field($this->extraFields, '//editedFacts/numFloors');
    }

    public function basement(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/basement');
    }

    public function roof(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/roof');
    }

    public function view(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/view');
    }

    public function parkingType(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/parkingType');
    }

    public function heatingSources(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/heatingSources');
    }

    public function heatingSystem(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/heatingSystem');
    }

    public function rooms(): ?string
    {
        return $this->field($this->extraFields, '//editedFacts/rooms');
    }

    public function descr(): ?string
    {
        return isset($this->extraFields->homeDescription) ? (string) $this->extraFields->homeDescription : null;
    }

    public function jsonSerialize()
    {
        return [
            'yearBuilt'         => $this->yearBuilt(),
            'areaSize'          => $this->areaSize(),
            'yardSize'          => $this->yardSize(),
            'bathroomsCount'    => $this->bathroomsCount(),
            'bedroomsCount'     => $this->bedroomsCount(),
            'lastSoldPrice'     => $this->lastSoldPrice(),
            'lastSoldDate'      => $this->lastSoldDate(),
            'taxAssessment'     => $this->taxAssessment(),
            'taxAssessmentYear' => $this->taxAssessmentYear(),
            'numFloors'         => $this->numFloors(),
            'basement'          => $this->basement(),
            'roof'              => $this->roof(),
            'view'              => $this->view(),
            'parkingType'       => $this->parkingType(),
            'heatingSources'    => $this->heatingSources(),
            'heatingSystem'     => $this->heatingSystem(),
            'rooms'             => $this->rooms(),
        ];
    }

    protected function field(?SimpleXMLElement $node, string $path): ?string
    {
        $value = $node ? Arr::first($node->xpath($path)) : null;

        return trim(strval($value)) !== '' ? strval($value) : null;
    }
}
