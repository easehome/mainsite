<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvestorsRequest;
use App\Http\Requests\SellRequest;
use App\Model\Orders;
use App\Model\Sells;
use App\User;
use App\Http\Requests\CallbackRequest;
use http\Env\Response;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Order\OrderHelper;

class OrderController extends Controller
{
    protected $password;

    public function index(SellRequest $request) {
        $post = $request->except('_token');

        //register user
        $user = OrderHelper::guard()->check() ? OrderHelper::guard()->user() : OrderHelper::registerUser($post);

        // create order
        OrderHelper::createOrder($user, $post['address']);

        //create sells
        $url = OrderHelper::createSell($user, $post['address']);

        if( Session::get('notfound',0) ) return redirect()->route('cabinet.home');
        return redirect()->route('cabinet.house_id', $url);
    }

    public function callback(CallbackRequest $request) {
        $post = $request->except('_token');

        try {
            Orders::create([
                'address' => $post['address'],
                'email'   => $post['email'],
                'type'    => 3
            ]);
        } catch (\Exception $e) {
            Log::error('CALLBACK',[$e->getMessage()]);
            return response()->json([
                'status' => 200,
                'error'  => 'Something wrong, call us by phone'
            ]);
        }

        return response()->json([
            'status'   => 200,
            'response' => 'The request is accepted we will write to you soon'
        ]);
    }

    public function investors(InvestorsRequest $request) {
        $post = $request->except('_token');

        try {
            Orders::create([
                'first_name' => $post['name'],
                'email' => $post['email'],
                'phone' => $post['phone'],
                'type'  => 2
            ]);
        }catch (\Exception $e) {
            Log::error('INVESTOR',[$e->getMessage()]);
            return response()->json([
                'status' => 200,
                'error'  => 'Something wrong, call us by phone'
            ]);
        }

        return response()->json([
            'status'   => 200,
            'response' => 'The request is accepted we will write to you soon'
        ]);
    }
}
