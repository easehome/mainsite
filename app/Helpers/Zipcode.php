<?php


namespace App\Helpers;


class Zipcode
{
    public static function getCitys($zip) {
        $response = self::request($zip);
        $citys = $response[0]['city_states'];
        $result = collect($citys)->map(function($item) {
            return $item['city'];
        });
        return $result->toArray();
    }

    protected static function request($zip) {
        $reqData = [
            'auth-id' => 'f5c87453-7847-d3bd-326a-5155251b02c1',
            'auth-token' => '3Q7mU1BaSgzBxozheG7Y',
            'zipcode' => $zip
        ];

        $url = "https://us-zipcode.api.smartystreets.com/lookup?";
        $request = file_get_contents($url.http_build_query($reqData));
        $response = json_decode($request, true);
        return $response;
    }
}
