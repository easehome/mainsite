@extends('layouts.app')

@section('title', "About us | ".env('APP_NAME'))
@section('description', "Description")

@section('content')
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="title">About us</h2>
                        <p class="text">How our Fee is Formed</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts.sections.about')

    @include('layouts.sections.metriks')
@endsection
