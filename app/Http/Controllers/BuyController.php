<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuyController extends Controller
{
    public function index() {
        $houses = [
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ],
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ],
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ],
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ],
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ],
            [
                'address' => '284 White Forks Apt 492',
                'link'    => 'card.html',
                'img'     => 'img/home-two/recent-3.jpg',
                'price'   => 1900,
                'specs'   => [
                    'bed'  => 1,
                    'bath' => 1
                ]
            ]
        ];
        $google_key = config('services.google.key');
        return view('buy.index', compact('houses','google_key'));
    }

    public function search(Request $request) {

        return response()->json([
            'status' => 200,
            'response' => []
        ]);
    }
}
