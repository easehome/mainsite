<?php

namespace App\Http\Controllers;

use App\Repository\RepositoryFactory;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BotApiController extends Controller
{
    public function property(Request $request)
    {
        Log::info('data property',$request->all());

        $address = $this->AddressGet($request);
        $city = $request->City ?? null;

        $repository = RepositoryFactory::getRepository();
        $property = $repository->getProperty($address, $city, $request->State, $request->Zip);

        if ($property === null) {
            return response()->json([
                'user_id' => $request->input('user_id'),
                'bot_id' => $request->input('bot_id'),
                'module_id' => $request->input('module_id'),
                'message' => __('bot.not_found'),
                'suggested_replies' => ['Try again']
            ], 200);
        }

        $cards = [];

        $houseImages = $property->images();

        if (!empty($houseImages)) {
            $gallery = [
                'type' => 'gallery',
                'value' => 'gallery'
            ];
            $countPhoto = 1;
            foreach($houseImages as $houseImage) {
                $gallery['gallery'][] = [
                    'image' => $houseImage,
                    'heading' => "Foto $countPhoto"
                ];
                $countPhoto++;
            }

            $cards[] = $gallery;
        }

        $estimatedPrice = $property->estimatedPrice();

        if (!empty($estimatedPrice)) {

            $formatEstimate = $this->formatEstimate($estimatedPrice);

            $cards[] = [
                "type" => "text",
                "value" => __('bot.estimate', $formatEstimate)
            ];
        }

        $datePlus21Day = $this->get21PlusDate();

        $cards[] = [
            "type" => "text",
            "value" => __('bot.closing_date', $datePlus21Day)
        ];

        $cards[] = [
            "type" => "text",
            "value" => __('bot.last_sentence')
        ];

        $propertyFields = $property->jsonSerialize();

        // remove empty
        $propertyFields = array_filter($propertyFields, function($value) {
            return $value !== null;
        });

        $fieldLabels = [
            'yearBuilt' => 'Year Built',
            'areaSize' => 'Area Size',
            'yardSize' => 'Yard Size',
            'estimatedPrice' => 'Estimated Price',
            'bedroomsCount' => 'Bedrooms Count',
            'bathroomsCount' => 'Bathrooms Count',
            'lastSoldDate' => 'Last Sold Date',
            'lastSoldPrice' => 'Last Sold Price',
            'taxAssessment' => 'Tax Assessment',
            'taxAssessmentYear' => 'Tax Assessment Year',
            'yearUpdated' => 'Year updated',
            'numFloors' => 'Number of floors',
            'basement' => 'Basement',
            'roof' => 'Roof type',
            'view' => 'View',
            'parkingType' => 'Parking type',
            'heatingSources' => 'Heating sources',
            'heatingSystem' => 'Heating system',
            'rooms' => 'Rooms'
        ];

        // format
        $propertyFields = array_map(function($name, $value) use ($fieldLabels) {
            return \Arr::get($fieldLabels, $name) . ': ' . ($value ?? 'no results');
        }, array_keys($propertyFields), $propertyFields);
        $propertyFields = implode("\n", $propertyFields);


        return response()->json([
            'user_id' => $request->input('user_id'),
            'bot_id' => $request->input('bot_id'),
            'module_id' => $request->input('module_id'),
            'message' => __('bot.pre_answer')."\n$propertyFields",
            'cards' => $cards,
            'suggested_replies' => ['Call me','New search']
        ]);
    }

    public function order(Request $request) {

        $post = $request->except('bot_id','channel','incoming_message','module_id','user_id');

        $message = '';
        foreach($post as $k=>$v) {
            $message .= "$k: $v".PHP_EOL;
        }

        Mail::raw($message, function ($message){
            $message->to( env('ORDER_TO', 'grvoyt@ya.ru') );
            $message->subject('order');
        });

        return response()->json([
            'user_id' => $request->input('user_id'),
            'bot_id' => $request->input('bot_id'),
            'module_id' => $request->input('module_id'),
            'message' => __('bot.phone_success'),
            'suggested_replies' => ['New search']
        ]);
    }

    public function manychat_order(Request $request) {

        $response = [
            'version' => 'v2',
            'content' => []
        ];

        $message = '';
        foreach($request->all() as $k=>$v) {
            $message .= "$k: $v".PHP_EOL;
        }

        Mail::raw($message, function ($message){
            $message->to( env('ORDER_TO', 'grvoyt@ya.ru') );
            $message->subject('order');
        });

        $response['content']['messages'][] = [
            'type' => 'text',
            'text' => __('bot.phone_success')
        ];

        $response['content']['quick_replies'][] = [
            'type' => 'node',
            'caption' => 'New search',
            'target'  => 'Start'
        ];

        return response()->json($response);
    }

    public function AddressGet(Request $request) {
        $post = $request->all();
        $address = trim($post['Address']);
        if(isset($post['Anumber']) && $post['Anumber'] !== 'false') $address .= " APT ".$post['Anumber'];
        return $address;
    }

    public function manychat(Request $request) {
        Log::debug('====manychat');
        Log::debug('data', $request->all());

        $address = $this->AddressGet($request);
        $city = $request->City ?? null;

        $repository = RepositoryFactory::getRepository();
        $property = $repository->getProperty($address, $city, $request->State, $request->Zip);

        $response = [
            'version' => 'v2',
            'content' => []
        ];

        if ($property === null) {
            $response['content']['messages'][] = [
                'type' => 'text',
                'text' => __('bot.not_found')
            ];
            $response['content']['quick_replies'][] = [
                'type' => 'node',
                'caption' => 'Try again',
                'target'  => 'State'
            ];
            return response()->json($response, 200);
        }

        $propertyFields = $property->jsonSerialize();

        // remove empty
        $propertyFields = array_filter($propertyFields, function($value) {
            return $value !== null;
        });

        $fieldLabels = [
            'yearBuilt' => 'Year Built',
            'areaSize' => 'Area Size',
            'yardSize' => 'Yard Size',
            'estimatedPrice' => 'Estimated Price',
            'bedroomsCount' => 'Bedrooms Count',
            'bathroomsCount' => 'Bathrooms Count',
            'lastSoldDate' => 'Last Sold Date',
            'lastSoldPrice' => 'Last Sold Price',
            'taxAssessment' => 'Tax Assessment',
            'taxAssessmentYear' => 'Tax Assessment Year',
            'yearUpdated' => 'Year updated',
            'numFloors' => 'Number of floors',
            'basement' => 'Basement',
            'roof' => 'Roof type',
            'view' => 'View',
            'parkingType' => 'Parking type',
            'heatingSources' => 'Heating sources',
            'heatingSystem' => 'Heating system',
            'rooms' => 'Rooms'
        ];

        // format
        $propertyFields = array_map(function($name, $value) use ($fieldLabels) {
            return \Arr::get($fieldLabels, $name) . ': ' . ($value ?? 'no results');
        }, array_keys($propertyFields), $propertyFields);
        $propertyFields = implode("\n", $propertyFields);

        $response['content']['messages'][] = [
            'type' => 'text',
            'text' => __('bot.pre_answer')."\n$propertyFields"
        ];

        $houseImages = $property->images();

        if (!empty($houseImages)) {
            $gallery = [];
            $countPhoto = 1;
            foreach($houseImages as $houseImage) {
                $gallery[] = [
                    'title' => "Foto $countPhoto",
                    'image_url' => $houseImage
                ];
                $countPhoto++;
            }

            $response['content']['messages'][] = [
                'type' => 'cards',
                'elements' => $gallery
            ];
        }

        $estimatedPrice = $property->estimatedPrice();

        if (!empty($estimatedPrice)) {

            $formatEstimate = $this->formatEstimate($estimatedPrice);

            $response['content']['messages'][] = [
                'type' => 'text',
                'text' => __('bot.estimate', $formatEstimate)
            ];
        }

        $datePlus21Day = $this->get21PlusDate();

        $response['content']['messages'][] = [
            'type' => 'text',
            'text' => __('bot.closing_date', $datePlus21Day)
        ];

        $response['content']['messages'][] = [
            'type' => 'text',
            'text' => __('bot.last_sentence')
        ];

        $response['content']['quick_replies'][] = [
            'type' => 'node',
            'caption' => 'Call me',
            'target' => 'CallMe'
        ];

        $response['content']['quick_replies'][] = [
            'type' => 'node',
            'caption' => 'New search',
            'target' => 'Start'
        ];
        return response()->json($response);
    }

    public function get21PlusDate() {
        $DatPlus21Day = Carbon::now()->addDays(21);
        $month = $DatPlus21Day->englishMonth;
        $day = $DatPlus21Day->day;
        $year = $DatPlus21Day->year;
        return compact('month','day','year');
    }

    public function formatEstimate($estimatedPrice) {
        $minPercent = 15;
        $maxPercent = 5;

        // obfuscate estimate
        $newEstimateMin = (int) ($estimatedPrice * ((100 - $minPercent) / 100));
        $newEstimateMax = (int) ($estimatedPrice * ((100 - $maxPercent) / 100));

        //format to American style
        $newEstimateMin = number_format($newEstimateMin);
        $newEstimateMax = number_format($newEstimateMax);

        return [
            'min' => $newEstimateMin,
            'max' => $newEstimateMax
        ];
    }
}
