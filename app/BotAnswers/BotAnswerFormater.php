<?php


namespace App\BotAnswers;


trait BotAnswerFormater
{
    public function toArray() {
        return get_object_vars($this);
    }

    public function toJSON() {
        return json_encode( $this->toArray() );
    }
}
